consul agent -server  -data-dir=/tmp/consul -node=agent-one -bind=172.20.20.11 -config-dir=/etc/consul.d



consul agent -server \
    -data-dir=/tmp/consul -node=agent-one -bind=172.28.128.4 \
    -config-dir=/etc/consul.d -join 172.28.128.3

consul agent -server \
    -data-dir=/tmp/consul -node=agent-three -bind=172.28.128.5 \
    -config-dir=/etc/consul.d -join 172.28.128.3



consul agent -data-dir=/tmp/consul -node=agent-four \
    -bind=172.28.128.6 -config-dir=/etc/consul.d


---

$ consul agent -server -bootstrap-expect=3     -data-dir=/tmp/consul -node=agent-one -bind=10.1.65.132 -config-dir=/etc/consul.d

---

$ consul agent -server     -data-dir=/tmp/consul -node=agent-two -bind=10.1.65.133     -config-dir=/etc/consul.d -join 10.1.65.132

---

$ consul agent -server     -data-dir=/tmp/consul -node=agent-three -bind=10.1.65.134 -config-dir=/etc/consul.d -join 10.1.65.133


---

$ consul agent -data-dir=/tmp/consul -node=agent-four  -bind=10.1.65.135 -config-dir=/etc/consul.d -join 10.1.65.132 -dns-port 53

---

$ consul agent -data-dir=/tmp/consul -node=agent-five  -bind=10.1.65.136 -config-dir=/etc/consul.d -join 10.1.65.135